## Core Java #

##### Introduction - (Wed Aug 5 2015)
* How to run create & run Java program?
* Primitive types
* How to use Literals
* Automatic type conversion in assignments
* Type casting
* Conditional Statements
* Looping Statements 


#### Java Collections 
* Map , Set
* Generics
* Static Class, Inner Class, Anonomous class

### Types of Programming
- Object Oriented Programming
- Functional Programming
- Aspect Oriented Programmin

### Java 8 Features
- [x] Default method in Interface
- [ ] Lambda expression
- [ ] Streams
- [ ] Optionals
- [ ] Date API
- [ ] Annotations

