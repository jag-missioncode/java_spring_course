import java.util.Arrays;

/**
 *  When staircase step size is 5, print the below pattern
 *      #
 *     ##
 *    ###
 *   ####
 *  #####
 */
public class Staircase {
    public static void main(String args[]){
        int stepSize = 5;
        buildStairCase(5);
        /*
        for (int column = 1; column <= stepSize; column++) {
            for (int row = 1; row <= stepSize; row++) {
                int blankSpace = stepSize - column; // When col = 1, Row = 1 , blankSpace = 5 -1
                if(row <= blankSpace){
                    System.out.print(" ");
                }else{
                    System.out.print("#");
                }
            }
            System.out.println("");
        } */
    }

    public static void buildStairCase(int step){
        char[] spaces = {' ', ' ', ' ', ' '};
        char[] stairs = {'#', '#', '#', '#', '#'};
        for (int i = 1; i <= step; i++) {
            System.out.print(Arrays.copyOf(spaces, step-i));
            System.out.println(Arrays.copyOf(stairs,i));
        }
    }
}
