public class AutomaticTypeConversion {
   public static void main(String args[]){
       long barcode = 100012355L;
       // Automatic type conversion
       double barcodeDouble = barcode;

       System.out.println("Long value " + barcode);
       System.out.println("Double value" + barcodeDouble);

       // long barcodeLongToDouble = barcodeDouble;

       short aptNumber = 127*2;
       //Type casting
       byte age = (byte)aptNumber;
       //short byteToShort = age;

       System.out.println(age);
   }
}
