public class Literals {

    public static void main(String args[]) {
    // long Literals - L
        //
        long itemNumber = 123231L;
        float price = 8.934f;

        int ssn = 123_45_6789;

        int color = 0xFF; // Hexadecimal
        byte areaCode = 011;

       // \t - tab
        // \n = next time

        // ____#
        // ---##
        // -###
        System.out.println("Tax in NC \t : 7.25%");

    }
}
