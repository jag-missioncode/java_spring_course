### Assignment 1 - Print the below output

Mon 03 | Tue 04 | Wed 05 | Thu 06 | Fri 07
------------------------------------------
85F    | 88F    | 89F   | 90F   | 91F
------------------------------------------

### Assignment 2 - Compute the distance to the lightning. 
Assume time interval is 7.2 seconds and sound travels approximately 1,100 feet per second.
To compute the distance, you will multiply time interval by sound speed.

### Assignment 3 - Find the number of duplicate characters in char array.
For an example char words = ['D','E','S','I', 'G', 'N','P','A','T','T','E','R','N','S'];
Display number of times each char is repeated.

### Assignment 4 - Find if username is already taken
Create an array of usernames for an example (user1, user2, user3, user4) 
and take username from commandline arguments.
Print the message "Username is already taken". if username is matching in usernames array
Print the message "Username is available". If username is not matching in usernames array

### Assignment 5 - Name generator
Create an array of characters from a to z with lowercase and uppercase
Generate random name from the char arrays and take word size as argument
For an example if word size is 5. Print name "sdsWE";

### Assignment 6 - Remove duplicate characters
For a given array of characters, remove all the duplicate characters and return unique character arrays
Ex. Input: ['P','R','O','G','R','A','M','M','I','N','G']
    Output: ['P','R','O','G','R','A','M','I','N','G']
    
### Assignment 7 - To Uppercase
For a given array of characters, change all the lowercase character to uppercase characters
Ex. Input: ['P','y','t','h','o','n']
    Output : ['P','Y','T','H','O','N']
    
### Assignment 8 - Find the median number
Ex. Input : [1,23,22,55,6,2,56]
    Output: 22
When there are 2 middle numbers median value will be average of them
Ex. Input : [24,6,69,1,97,56,3,9]
    Output : 9 + 24 / 2 = 16.5 = 17

### Assignment 9 - format number
Ex Input : 123456789, format : xxx-xx-xxxx
    Output : 123-45-6789
    
### Assignment 10 - format string
Ex Input: "Aug082020", format :"XXX XX,XXXX"
    Output : Aug 02,2020
    