package com.core.exceptions;

public class UserNotFoundException extends Exception {
    public UserNotFoundException(String message){
        // Call parent constructor first
        super(message);
    }
}
