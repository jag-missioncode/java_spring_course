package com.core.main;

import com.core.collections.generics.ProductSet;
import com.core.collections.UserMap;
import com.core.exceptions.UserNotFoundException;
import com.core.store.*;

import java.util.*;

public class Shop {

    static class ShopConstants{
        public final static String WELCOME_GUEST_STRING = "Welcome Guest";
    }

    static class Setup{
        public UserMap loadUsers(){
            UserMap users = new UserMap();
            users.add(new User("Jagadesh", "Kannan", "jkannan"));
            users.add(new User("Harish", "Babu", "hbabu"));
            users.add(new User("Sai", "Deepuru", "sai"));
            return users;
        }

        public ProductSet loadProducts(){

            ProductSet<Product> products = new ProductSet();
            products.add(new Product( "iPhone", "IOS Mobile phone", 1200.23));
            products.add(new Product( "samsung", "Android Mobile phone", 980.23));
            products.add(new Product( "Monitor", "Computer monitor", 400.23));
            products.add(new Product( "MacPro", "IOS Laptop", 2200.23));
            products.add(new Product( "Tablet", "IOS Tablet", 600.23));
            products.add(new Product( "samsung", "Android Mobile phone", 980.23));
            products.add(new Product( "MacPro", "IOS Laptop", 2200.23));

            return products;
        }

        public ProductSet loadMemberOnlyProducts(){

            ProductSet<MembersOnlyProduct> membersonlyProducts = new ProductSet();
            membersonlyProducts.add(new MembersOnlyProduct("MacMini", "IOS small super computer", 299.99));

            return membersonlyProducts;
        }

        public ProductSet loadSeasonalProducts(){

            ProductSet<SeasonalProduct> seasonalProducts = new ProductSet<>();
            seasonalProducts.add(new SeasonalProduct("ExecutiveChair", "Luxury chair", 689.88));

            return seasonalProducts;
        }

    }

    public static void main(String[] args) {
        Setup setup = new Shop.Setup();
        UserMap users = setup.loadUsers();
        ProductSet products = setup.loadProducts();

        System.out.println("Please enter your userId");
        Scanner userInput = new Scanner(System.in);
        User matchedUser = null;
        try{
            matchedUser = users.findByUserId(userInput.next());
        }catch (UserNotFoundException ex){
            System.out.println(ShopConstants.WELCOME_GUEST_STRING);
        }

        if(matchedUser != null){
            System.out.println("Welcome "+ matchedUser.getFullName());
        }

        setupShop(matchedUser, products, setup.loadMemberOnlyProducts(), setup.loadSeasonalProducts());
    }

    public static void setupShop(User loggedInUser, ProductSet<Product> products, ProductSet<MembersOnlyProduct> membersOnlyProductSet, ProductSet<SeasonalProduct> seasonalProductSet){
        System.out.println("================ Regular Products ==============");
        // Iterator<Product> productIterator = products.getAllProductSet().iterator();

        ArrayList<Product> allProducts = (ArrayList<Product>) products.getAllProductList();
        /*allProducts.sort(new Comparator<Product>() {
            @Override
            public int compare(Product o1, Product o2) {
                return o2.getName().toLowerCase().compareTo(o1.getName().toLowerCase());
            }
        });*/

        Collections.sort(allProducts,(p1, p2)-> p2.getName().compareTo(p1.getName()));

        for (Product product : allProducts){
            System.out.println(product.getName());
        }

        if(loggedInUser != null){
            System.out.println("================ Members Only Products ==============");

            for (MembersOnlyProduct membersOnlyProduct : membersOnlyProductSet.getAllProductList()){
                System.out.println(membersOnlyProduct.getName());
            }
        }
        System.out.println("================ Seasonal Products ==============");

        for (SeasonalProduct seasonalProduct : seasonalProductSet.getAllProductList()){
            System.out.println(seasonalProduct.getName());
        }

        /*while (productIterator.hasNext()){
            Product product = productIterator.next();
            System.out.println(product.getName());
        }*/

        /*for (int i = 0; i < products.getAllProductSet(); i++) {
            Product product = products.getAllProducts().get(i);
            System.out.println((i+1) + ".) " + product.getName());
        }*/

       /* System.out.println("Tip: Type 0 to quit");
        boolean hasUserInput = true;
        Scanner menuInput = null;
        while (hasUserInput){
            menuInput = new Scanner(System.in);

            int userSelectedChoice = menuInput.nextInt();
            List<Product> allProducts = products.getAllProducts();

            switch (userSelectedChoice){
                case 0 : System.out.println("Exit"); hasUserInput = false; menuInput.close(); break;
                case 1 : System.out.println(allProducts.get(0).toString()); break;
                case 2 :
                    System.out.println(allProducts.get(1).toString()); break;
                case 3 : System.out.println(allProducts.get(2).toString()); break;
                case 4 : System.out.println(allProducts.get(3).toString()); break;
                default:
                    System.out.println("Please enter 1 to 5");
            }
        }

*/
    }


}
