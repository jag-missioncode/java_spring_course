package com.core.util;

public class UniqueNumberGenerator implements UniqueId{
    public static int generateNextId(){
        return (int)Math.round(Math.random()*100000);
    }

    public static String generateNext(){
        return "123";
    }

    public static String generateNext(String prefix){
        int uuid = generateNextId();
        return prefix.concat(Integer.toString(uuid));
    }

    public static String generateNext(String prefix, String suffix){
        double uuid = generateNextId();
        return prefix.concat(Double.toString(uuid)).concat(suffix);
    }

    @Override
    public String getId() {
        return generateNext("NUM-");
    }
}
