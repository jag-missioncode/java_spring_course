package com.core.util;

public interface UniqueId {
    int MIN_LENGTH = 3;

    public String getId();

    default public boolean isValid(){
        return true;
    }
}
