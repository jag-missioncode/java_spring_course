package com.core.util;

public class UniqueIdGenerator implements UniqueId{

    private int MAX_LENGTH = 5;
    private String id;

    public UniqueIdGenerator(int MAX_LENGTH){
        this.MAX_LENGTH = MAX_LENGTH;
    }

    private String generateString(){
        return "ABC123";
    }

    @Override
    public String getId() {
        return this.generateString();
    }

    @Override
    public boolean isValid() {
        return false;
    }
}
