package com.core.store;

import com.core.exceptions.UserNotFoundException;

import java.util.ArrayList;
import java.util.List;

public class Users {
    List users;

    public Users(){
        this.users = new ArrayList();
    }

    public void add(User user){
        this.users.add(user);
    }

    public User findByUserId(String userId) throws UserNotFoundException {
        // For each loop - Iterator - O(N)
        for (Object usrObj: users){
            User user = (User) usrObj;
            if(user.getUserId().equals(userId)){
                return user;
            }
        }
        throw new UserNotFoundException("User is not found");
    }

}
