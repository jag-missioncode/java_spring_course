package com.core.store;

import com.core.util.UniqueIdGenerator;

public class User {
    private String id;
    private String firstName;
    private String lastName;
    private String userId;

    public User(String firstName, String lastName, String userId) {
        UniqueIdGenerator userUniqueId = new UniqueIdGenerator(5);
        this.id = userUniqueId.getId();
        this.firstName = firstName;
        this.lastName = lastName;
        this.userId = userId;
    }

    public String getFullName(){
        return this.firstName + " " + this.lastName;
    }

    public String getUserId(){
        return this.userId;
    }
}

