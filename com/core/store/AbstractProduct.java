package com.core.store;

public abstract class AbstractProduct {
    private String id;
    private String name;
    private String description;
    private double price;
    private double basePrice;
    private Status status;

    public double getBasePrice() {
        return basePrice;
    }

    public AbstractProduct(String id, String name, String description, double basePrice) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.basePrice = basePrice;
        this.status = Status.DRAFT;
    }

    public String getId(){
        return this.id;
    }

    abstract public double getPrice();

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public int hashCode() {
        // System.out.println("Product hashcode : " + this.getName() + "\t" + this.getName().hashCode());
        return this.getName().hashCode() * 13;
    }

    @Override
    public boolean equals(Object obj) {
        Product product = (Product)obj;
        if(this.name.equals(product.getName())){
            return true;
        }
        return false;
    }
}
