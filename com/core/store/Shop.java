package com.core.store;

import com.core.exceptions.UserNotFoundException;

import java.util.List;
import java.util.Scanner;

public class Shop {
    public static void main(String[] args) {

        Users users = new Users();
        users.add(new User("Jagadesh", "Kannan", "jkannan"));
        users.add(new User("Harish", "Babu", "hbabu"));
        users.add(new User("Sai", "Deepuru", "sai"));

        System.out.println("Please enter your userId");
        Scanner userInput = new Scanner(System.in);
        User matchedUser = null;
        try{
            matchedUser = users.findByUserId(userInput.next());
        }catch (UserNotFoundException ex){
            System.out.println("Welcome Guest");
        }

        if(matchedUser != null){
            System.out.println("Welcome "+ matchedUser.getFullName());
        }

        setupShop();

    }

    public static void setupShop(){

        Products products = new Products();
        products.add(new Product( "iPhone", "IOS Mobile phone", 1200.23));
        products.add(new Product( "samsung", "Android Mobile phone", 980.23));
        products.add(new Product( "Monitor", "Computer monitor", 400.23));
        products.add(new Product( "MacPro", "IOS Laptop", 2200.23));
        products.add(new Product( "Tablet", "IOS Tablet", 600.23));


        System.out.println("================ Main Menu ==============");

        for (int i = 0; i < products.getAllProducts().size(); i++) {
            Product product = products.getAllProducts().get(i);
            System.out.println((i+1) + ".) " + product.getName());
        }

        System.out.println("Tip: Type 0 to quit");
        boolean hasUserInput = true;
        Scanner menuInput = null;
        while (hasUserInput){
            menuInput = new Scanner(System.in);

            int userSelectedChoice = menuInput.nextInt();
            List<Product> allProducts = products.getAllProducts();

            switch (userSelectedChoice){
                case 0 : System.out.println("Exit"); hasUserInput = false; menuInput.close(); break;
                case 1 : System.out.println(allProducts.get(0).toString()); break;
                case 2 :
                    System.out.println(allProducts.get(1).toString()); break;
                case 3 : System.out.println(allProducts.get(2).toString()); break;
                case 4 : System.out.println(allProducts.get(3).toString()); break;
                default:
                    System.out.println("Please enter 1 to 5");
            }
        }


    }

}
