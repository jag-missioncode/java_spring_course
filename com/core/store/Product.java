package com.core.store;

import com.core.util.UniqueId;
import com.core.util.UniqueIdGenerator;
import com.core.util.UniqueNumberGenerator;

import java.lang.annotation.Documented;
import java.util.Objects;

public class Product extends AbstractProduct{

   public Product(String name, String description, double price){
       super(UniqueNumberGenerator.generateNext("PRO-"), name, description, price);
   }

    @Override
    public double getPrice() {
        return 10.0;
    }
}
