package com.core.store;

import java.util.ArrayList;
import java.util.List;

public class Products {

    private List products;

    public Products(){
        this.products = new ArrayList();
    }

    public void add(Product product){
        this.products.add(product);
    }

    public void remove(Product product){
        this.products.remove(product);
    }

    public List<Product> getAllProducts(){
        return this.products;
    }

}
