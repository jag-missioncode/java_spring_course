package com.core.store;

import com.core.util.UniqueNumberGenerator;

public class MembersOnlyProduct extends AbstractProduct {

    public MembersOnlyProduct(String name, String description, double basePrice) {
        super(UniqueNumberGenerator.generateNext("MEM-PRO-"), name, description, basePrice);
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public double getPrice() {
        return this.getBasePrice() - this.getBasePrice() * 0.05;
    }
}
