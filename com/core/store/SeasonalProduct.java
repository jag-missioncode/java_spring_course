package com.core.store;

import com.core.util.UniqueNumberGenerator;

import java.util.Date;

public class SeasonalProduct extends AbstractProduct{

    private Date startDate;
    private Date endDate;

    public SeasonalProduct(String name, String description, double price) {
        super( UniqueNumberGenerator.generateNext("SES-PRO-"), name, description, price);
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        // if startDate == currentDate, then setStatus as ACTIVE
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        // if endDate > currentDate, then setStatus as INACTIVE
        this.endDate = endDate;
    }

    @Override
    public double getPrice() {
        return this.getBasePrice() + this.getBasePrice()*0.05;
    }
}
