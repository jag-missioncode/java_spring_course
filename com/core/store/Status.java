package com.core.store;

public enum Status {
    DRAFT,
    PUBLISHED,
    ACTIVE,
    INACTIVE,
    EXPIRED,
    REMOVED
}