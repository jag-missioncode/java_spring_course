package com.core.collections;

import com.core.exceptions.UserNotFoundException;
import com.core.store.User;

import java.util.HashMap;
import java.util.Map;

public class UserMap {
    private Map<String, User> usersMap;

    public UserMap(){
        this.usersMap = new HashMap<>();
    }

    public void add(User user){
        this.usersMap.put(user.getUserId(), user);
    }

    public User findByUserId(String userId) throws UserNotFoundException {
        // O(1)
        User matchedUser = this.usersMap.get(userId);
        if(matchedUser == null){
            throw new UserNotFoundException("User is not found");
        }
        return matchedUser;
    }
}
