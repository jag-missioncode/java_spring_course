package com.core.collections.generics;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ProductSet<E> {

    private Set<E> products;

    public ProductSet(){
        this.products = new HashSet<>();
    }

    public void add(E product){
        this.products.add(product);
    }

    public void remove(E product){
        this.products.remove(product);
    }

    public Set<E> getAllProductSet(){
        return this.products;
    }

    public List<E> getAllProductList(){
        return new ArrayList<>(this.products);
    }

}
