package com.core.collections;

import com.core.store.Product;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ProductSet {
    private Set<Product> products;

    public ProductSet(){
        this.products = new HashSet<>();
    }

    public void add(Product product){
        this.products.add(product);
    }

    public void remove(Product product){
        this.products.remove(product);
    }

    public Set<Product> getAllProductSet(){
        return this.products;
    }

    public List<Product> getAllProductList(){
        return new ArrayList<>(this.products);
    }
}
