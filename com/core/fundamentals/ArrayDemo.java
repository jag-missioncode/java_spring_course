package com.core.fundamentals;

/**
 * Package are important to group the classes
 *
 */


public class ArrayDemo {

    public static void main(String args[]){
        // int userEnteredWeather = args[0];
        System.out.println("User entered value" + args[0]);
        int todayWeather = 0;
        try{
            todayWeather = Integer.parseInt(args[0]);
        }catch (NumberFormatException nfe){
            System.out.println("Please enter only numbers");
        }
        int[] currentWeekWeather = {80, 90, 76, 77, 88};
        boolean isWeatherMatching = contains(currentWeekWeather, todayWeather);
        if(isWeatherMatching){
            System.out.println("Today Weather temp matched in our current week weather");
        }
    }
    /**
     * Contains method with 2 arguments
     * 1. Array of values
     * 2. Element to be found
     *
     * Return boolean
     * DRY - Dont Repeat Yourself
     */
    public static boolean contains(int[] numbers, int value){
        int totalNumbers = numbers.length;
        for (int i = 0; i < totalNumbers; i++) {
            if(numbers[i] == value){
                return true;
            }
        }
        return false;
    }
}
