package com.core.java8;

import com.core.collections.generics.ProductSet;
import com.core.collections.UserMap;
import com.core.exceptions.UserNotFoundException;
import com.core.store.*;
import com.core.util.UniqueIdGenerator;

import java.lang.annotation.Documented;
import java.util.*;

public class Shop {

    private static ArrayList dependecies = new ArrayList();

    static class ShopConstants{
        public final static String WELCOME_GUEST_STRING = "Welcome Guest";
    }

    public static void main(String[] args) {
        Setup setup = Setup.getInstance();
        UserMap users = setup.getAllUsers();

        System.out.println("Please enter your userId");
        Scanner userInput = new Scanner(System.in);
        Optional<User> matchedUser = Optional.empty();
        try{
            matchedUser = Optional.ofNullable(users.findByUserId(userInput.next()));
        }catch (UserNotFoundException ex){
            System.out.println(ShopConstants.WELCOME_GUEST_STRING);
        }
        matchedUser.orElse(new User("Guest","", "GUEST"));
        matchedUser.ifPresent(user -> System.out.println(user.getFullName()));

        setupShop(matchedUser);
    }

    public static void setupShop(Optional<User> loggedInUser){
        Setup setup = Setup.getInstance();
        ProductSet<Product> products = setup.getProducts();

        System.out.println("================ Regular Products ==============");
        // Iterator<Product> productIterator = products.getAllProductSet().iterator();
        ProductCatalog productCatalog = new ProductCatalogBuilder()
                                        .setRegularProducts(setup.getProducts())
                                        .setSeasonalProducts(setup.getSeasonalProducts())
                                        .build();

        productCatalog.printMenu();
    }


}
