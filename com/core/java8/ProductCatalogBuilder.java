package com.core.java8;

import com.core.collections.generics.ProductSet;
import com.core.store.MembersOnlyProduct;
import com.core.store.Product;
import com.core.store.SeasonalProduct;

public class ProductCatalogBuilder {

    private ProductSet<MembersOnlyProduct> membersonlyProducts;
    private ProductSet<SeasonalProduct> seasonalProducts;
    private ProductSet<Product> regularProducts;

    public ProductCatalogBuilder setMemberOnlyProducts(ProductSet<MembersOnlyProduct> membersonlyProducts){
        this.membersonlyProducts = membersonlyProducts;
        return this;
    }
    public ProductCatalogBuilder setRegularProducts(ProductSet<Product> regularProducts){
        this.regularProducts = regularProducts;
        return this;
    }
    public ProductCatalogBuilder setSeasonalProducts(ProductSet<SeasonalProduct> seasonalProducts){
        this.seasonalProducts = seasonalProducts;
        return this;
    }

    public ProductCatalog build(){
        //TODO
       return new ProductCatalog(this.membersonlyProducts,this.seasonalProducts, this.regularProducts);
    }
}
