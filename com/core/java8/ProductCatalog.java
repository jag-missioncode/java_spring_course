package com.core.java8;

import com.core.collections.generics.ProductSet;
import com.core.store.MembersOnlyProduct;
import com.core.store.Product;
import com.core.store.SeasonalProduct;

import java.util.List;

public class ProductCatalog {
    private ProductSet<MembersOnlyProduct> membersonlyProducts = null;
    private ProductSet<SeasonalProduct> seasonalProducts = null;
    private ProductSet<Product> regularProducts = null;

    public ProductCatalog(ProductSet<MembersOnlyProduct> membersonlyProducts, ProductSet<SeasonalProduct> seasonalProducts, ProductSet<Product> regularProducts) {
        this.membersonlyProducts = membersonlyProducts;
        this.seasonalProducts = seasonalProducts;
        this.regularProducts = regularProducts;
    }

    public void printMenu(){
        this.printProducts(this.regularProducts.getAllProductList());
        this.printProducts(this.regularProducts.getAllProductList());
        this.printProducts(this.regularProducts.getAllProductList());
    }

    private void printProducts(List<Product> products){
        if(products != null){
            products.stream()
                    .map(product -> product.getName())
                    .filter(productName -> productName.startsWith("M"))
                    .sorted((p1,p2)-> p1.toLowerCase().compareTo(p2.toLowerCase()))
                    .forEach(System.out::println);
        }
    }
}
