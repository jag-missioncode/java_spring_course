package com.core.java8;

import com.core.collections.UserMap;
import com.core.collections.generics.ProductSet;
import com.core.store.MembersOnlyProduct;
import com.core.store.Product;
import com.core.store.SeasonalProduct;
import com.core.store.User;

/**
 * Singleton Design pattern
 */
public class Setup {

    private static Setup setupInstance = null;
    private UserMap users = null;

    private ProductSet<Product> products = null;
    private ProductSet<SeasonalProduct> seasonalProducts = null;

    public ProductSet<SeasonalProduct> getSeasonalProducts() {
        return seasonalProducts;
    }

    /**
     * Initial Phase
     */
        private Setup(){
            this.loadUsers();
            this.loadProducts();
            this.loadSeasonalProducts();
        }

        public static Setup getInstance(){
            if(setupInstance == null){
                setupInstance = new Setup();
            }
            return setupInstance;
        }

        public void destroyInstance(){
            this.setupInstance = null;
        }

        private void loadUsers(){
            this.users = new UserMap();
            users.add(new User("Jagadesh", "Kannan", "jkannan"));
            users.add(new User("Harish", "Babu", "hbabu"));
            users.add(new User("Sai", "Deepuru", "sai"));
        }

        public UserMap getAllUsers(){
            return this.users;
        }

        public ProductSet<Product> getProducts() {
            return products;
        }

        private void loadProducts(){
            this.products = new ProductSet();
            products.add(new Product( "iPhone", "IOS Mobile phone", 1200.23));
            products.add(new Product( "samsung", "Android Mobile phone", 980.23));
            products.add(new Product( "Monitor", "Computer monitor", 400.23));
            products.add(new Product( "MacPro", "IOS Laptop", 2200.23));
            products.add(new Product( "Tablet", "IOS Tablet", 600.23));
            products.add(new Product( "samsung", "Android Mobile phone", 980.23));
            products.add(new Product( "MacPro", "IOS Laptop", 2200.23));

        }


        private ProductSet loadMemberOnlyProducts(){

            ProductSet<MembersOnlyProduct> membersonlyProducts = new ProductSet();
            membersonlyProducts.add(new MembersOnlyProduct("MacMini", "IOS small super computer", 299.99));

            return membersonlyProducts;
        }

        private void loadSeasonalProducts(){

            this.seasonalProducts = new ProductSet<>();
            seasonalProducts.add(new SeasonalProduct("ExecutiveChair", "Luxury chair", 689.88));

        }

}
