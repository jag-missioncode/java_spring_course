public class PrimitiveDataType {
    public static void main(String args[]){

        /* Integer Primitive data Types */

        // 8 bits and Range -128 to 127
        byte age = 19;

        // Short : 16 bits and Range -32,768 to 32,767
        short price = 9000;

        short aptNumber = 808;

        //int : 32 bits and -2,147,483, 648 to 2,147,483,647
        int itemNumber = 1235232;

        //long : 64 bits and 2^64

        long barcode = 198093423687L;

        boolean isActiveUser = true;

        char gender = 'M'; // M / F / O

    }
}
